﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using Rewired;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public static PlayerMovement Instance;
    
    private Player player;
    private Rigidbody rb;

    public float moveSpeed = 2f;

    public CharacterAnimations CharacterAnimations;

    private void Awake() {
        Instance = this;
    }
    
    private void Start() {
        player = ReInput.players.GetPlayer(0);
        rb = GetComponent<Rigidbody>();
    }
    
    private void Update() {
        if (DialogueDisplay.isDisplayed) return;
        if (InventoryToggle.isOpen) return;
        
        float x = player.GetAxis("MoveX");
        float y = player.GetAxis("MoveY");
        Vector2 vec = new Vector2(x, y);
        Vector2 dir = vec.normalized;
        float mag = Mathf.Min(vec.magnitude, 1f);
        vec = dir * mag;
        vec = RotateVector(vec, 47f);
        Vector3 r = rb.velocity;
        r.x = vec.x * moveSpeed;
        r.z = vec.y * moveSpeed;
        rb.velocity = r;

        if (transform.position.y < -1.85f) {
            LaboratoryController.Instance.StartOpening();
        }
        else {
            LaboratoryController.Instance.StartClosing();
        }

        if (transform.position.y < -4.25f) {
            StairsController.Instance.StartOpening();
        }
        else {
            StairsController.Instance.StartClosing();
        }
        
        if (transform.position.y < -3.5f) {
            BasementRoomController.Instance.StartClosing();
        }
        else {
            BasementRoomController.Instance.StartOpening();
        }
        
        if (transform.position.y < -0.5f) {
            StairwellRoomController.Instance.StartClosing();
        }
        else {
            StairwellRoomController.Instance.StartOpening();
        }
        
        CharacterAnimations.WalkAnimation((new Vector2(r.x, r.z)).magnitude);
        CharacterAnimations.UpdateFacing(x);
    }
    
    public static Vector2 RotateVector (Vector2 v, float degrees) {
        float ca = Mathf.Cos(degrees * Mathf.Deg2Rad);
        float sa = Mathf.Sin(degrees * Mathf.Deg2Rad);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }
    
}
