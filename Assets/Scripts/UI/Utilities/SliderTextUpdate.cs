﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderTextUpdate : MonoBehaviour {

    public InputField textObject;
    public Slider sliderObject;

    private void Awake()
    {
        sliderObject.onValueChanged.AddListener(UpdateText);
        textObject.onEndEdit.AddListener(UpdateSlider);
    }

    void OnEnable()
    {
        UpdateText(sliderObject.value);
    }

    public void UpdateText(float value)
    {
        float textValue = value;
        textObject.text = textValue.ToString("0.##");
    }

    public void UpdateSlider(string value)
    {
        sliderObject.onValueChanged.RemoveListener(UpdateText);
        float sliderValue = float.Parse(value);
        sliderObject.value = sliderValue;
        sliderObject.onValueChanged.AddListener(UpdateText);
    }
}
