﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class OptionList : MonoBehaviour {

    public TextMeshProUGUI textObject;
    public List<string> options = new List<string>(new string[] { "Option A", "Option B", "Option C" });
    private int value = 0;

    [System.Serializable]
    public class OptionEvent : UnityEvent<int> { }
    public OptionEvent onValueChanged = new OptionEvent();

    public int Value
    {
        get
        {
            return value;
        }

        set
        {
            this.value = value;
            SetOption();
        }
    }

    private void OnEnable()
    {
        SetOption();
    }

    void SetOption()
    {
        value = Mathf.Clamp(Value, 0, options.Count - 1);
        if (value >= options.Count || value < 0) return;
        textObject.text = options[Value];
        onValueChanged.Invoke(Value);
    }

    public void NextOption()
    {
        value++;
        if (Value >= options.Count) Value = 0;
        SetOption();
    }

    public void PreviousOption()
    {
        value--;
        if (Value < 0) Value = options.Count - 1;
        SetOption();
    }

    public void AddOptions(List<string> targetOptions)
    {
        options.AddRange(targetOptions);
    }
}
