﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(LayoutElement))]
public class LayoutDimension : MonoBehaviour {

    private LayoutElement element;
    private RectTransform textElement;

    public bool checkWidth;
    public float maxWidth;

    public bool checkHeight;
    public float maxHeight;

    private void Awake()
    {
        element = GetComponent<LayoutElement>();
        textElement = GetComponent<RectTransform>();
    }

    private void OnDisable()
    {
        ResetSize();
    }

    private void Update()
    {
        if (checkWidth && textElement.rect.width >= maxWidth) element.preferredWidth = maxWidth;
        else element.preferredWidth = -1;

        if (checkHeight && textElement.rect.height >= maxHeight) element.preferredHeight = maxHeight;
        else element.preferredHeight = -1;
    }

    public void ResetSize()
    {
        element.preferredWidth = -1;
        element.preferredHeight = -1;
    }

}
