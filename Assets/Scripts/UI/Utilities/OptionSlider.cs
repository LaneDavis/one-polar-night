﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionSlider : MonoBehaviour {

    public Slider targetSlider;
    public float changeAmount = 1;

    public void IncreaseValue()
    {
        targetSlider.value += changeAmount;
    }

    public void DecreaseValue()
    {
        targetSlider.value -= changeAmount;
    }
}
