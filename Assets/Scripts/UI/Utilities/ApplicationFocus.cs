﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ApplicationFocus : MonoBehaviour {

    public bool onlyInBuild;

    public UnityEvent OnGainFocus;
    public UnityEvent OnLossFocus;

    void Awake()
    {
        if (onlyInBuild && Application.isEditor) Destroy(this);
    }

	void OnApplicationFocus(bool focusStatus)
    {
        if (focusStatus) OnGainFocus.Invoke();
        else OnLossFocus.Invoke();
    }
}
