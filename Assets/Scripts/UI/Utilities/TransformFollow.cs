﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformFollow : MonoBehaviour {

    public Transform targetTransform;
    public float smoothing = 1;
    public float influence = 1;

    public Vector3 offset;

    private void LateUpdate()
    {
        if (targetTransform == null) return;

        Vector3 desiredPosition = (influence * targetTransform.localPosition) + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.localPosition, desiredPosition, smoothing * Time.deltaTime);

        transform.localPosition = smoothedPosition;
    }

    public void QuickSnap()
    {
        Vector3 desiredPosition = (influence * targetTransform.localPosition) + offset;
        transform.localPosition = desiredPosition;
    }

}
