﻿using UnityEngine;
using System.Collections;
using Rewired;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EventSystemInputSelector : MonoBehaviour {

    private GameObject previousGameObject;
    private ControllerType previousController;

	void Update()
    {
        CheckSelection();
        Controller controller = ReInput.controllers.GetLastActiveController();

        if(controller != null)
        {
            if(previousController != controller.type)
            {
                switch (controller.type)
                {
                    case ControllerType.Joystick:
                        SetMouseInput(false);
                        ShowSelection(true);
                        break;
                    case ControllerType.Keyboard:
                        SetMouseInput(false);
                        ShowSelection(true);
                        break;
                    case ControllerType.Mouse:
                        SetMouseInput(true);
                        ShowSelection(false);
                        break;
                    default:
                        SetMouseInput(true);
                        ShowSelection(false);
                        break;
                }
                previousController = controller.type;
            }
        }
    }

    void ShowSelection(bool value)
    {
        if(value)
        {
            ExecuteEvents.Execute<ISelectHandler>(previousGameObject, new BaseEventData(EventSystem.current), ExecuteEvents.selectHandler);
            EventSystem.current.SetSelectedGameObject(previousGameObject);
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    void SetMouseInput(bool value)
    {
        Cursor.visible = value;
        GetComponent<GraphicRaycaster>().enabled = value;
    }

    void CheckSelection()
    {
        if (EventSystem.current.currentSelectedGameObject != null) previousGameObject = EventSystem.current.currentSelectedGameObject;
    }
}
