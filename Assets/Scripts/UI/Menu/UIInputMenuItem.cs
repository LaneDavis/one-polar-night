﻿using UnityEngine;
using System.Collections;
using Rewired;
using UnityEngine.Events;

public class UIInputMenuItem : MonoBehaviour {

	[Range(1,4)] public int playerNumber = 1;
    public bool allPlayers;
    public bool allControllers;
    public bool onlySystemPlayer;
	public Player targetPlayer;
	public string inputName;

	public float deadzone = .1f;
	public float inputActionsDuration = .2f;
    public float inputDelay = .5f;
	public UnityEvent OnButtonDown;
	public UnityEvent OnPositiveAxis;
	public UnityEvent OnNegativeAxis;
	public UnityEvent OnIdleAxis;
    public UnityEvent OnAnyButtonDown;

    private int targetInputIndex;

	private float timer = 0;
    private bool delayed;
    private bool delayFlag;
    private bool firstMoveInput;
    private bool idleFlag;

	void OnEnable()
	{
		targetPlayer = ReInput.players.Players[playerNumber - 1];
	}

    void Update()
    {
        if (allControllers && allPlayers)
        {
            CheckControllers();
            //CheckPlayers();
        }
        else if(allControllers)
        {
            CheckControllers();
        }
        else if (allPlayers)
        {
            CheckInputs();
        }
        else
        {
            CheckInputs(targetPlayer);
        }
	}

    void CheckControllers()
    {
        //foreach (Controller controller in ReInput.controllers.Controllers)
        //{
        if (onlySystemPlayer)
        {
            CheckInputs(ReInput.players.SystemPlayer);
        }
        else
        {
            CheckInputs();
        }
        //}
    }

    //void CheckPlayers()
    //{
    //    CheckInputs();
    //}

    void CheckInputs()
    {
        if (AnyPlayerButtonDown())
        {
            OnButtonDown.Invoke();
        }

        CheckMovement();

        if (AnyPlayerAnyButtonDown())
        {
            OnAnyButtonDown.Invoke();
        }
    }

    void CheckInputs(Player player)
    {
        if (player.GetButtonDown(inputName))
        {
            OnButtonDown.Invoke();
        }

        CheckMovement(player);

        if (player.GetAnyButtonDown())
        {
            OnAnyButtonDown.Invoke();
        }
    }

    bool AnyPlayerButtonDown()
    {
        bool isDown = false;
        foreach (Player player in ReInput.players.AllPlayers)
        {
            if (player.GetButtonDown(inputName)) isDown = true;
        }

        return isDown;
    }

    bool AnyPlayerAnyButtonDown()
    {
        bool isDown = false;
        foreach (Player player in ReInput.players.AllPlayers)
        {
            if (player.GetAnyButtonDown()) isDown = true;
        }

        return isDown;
    }

    float AnyPlayerAxisRaw()
    {
        float moveFloat = 0;

        foreach (Player player in ReInput.players.AllPlayers)
        {
            moveFloat += player.GetAxisRaw(inputName);
        }

        moveFloat = Mathf.Clamp(moveFloat, -1, 1);

        return moveFloat;
    }

    void CheckMovement()
    {
        float moveFloat = AnyPlayerAxisRaw();
        bool isMoving = (moveFloat >= deadzone || moveFloat <= -deadzone);

        if (!isMoving)
        {
            firstMoveInput = false;
            delayFlag = false;
            timer = 0;
            if (!idleFlag)
            {
                OnIdleAxis.Invoke();
                idleFlag = true;
                return;
            }
        }
        else
        {
            idleFlag = false;

            if (!firstMoveInput)
            {
                firstMoveInput = true;
                InvokeMovement(moveFloat);
                return;
            }

            timer += Time.unscaledDeltaTime*2;
            bool actionTimePassed = (timer >= inputActionsDuration);
            bool delayTimePassed = (timer >= inputDelay);

            if (!delayFlag)
            {
                if (delayTimePassed)
                {
                    timer = 0;
                    delayFlag = true;
                    InvokeMovement(moveFloat);
                }
            }
            else
            {
                if (actionTimePassed)
                {
                    timer = 0;
                    InvokeMovement(moveFloat);
                }
            }
        }
    }

    void CheckMovement(Player player)
    {
        float moveFloat = player.GetAxisRaw(inputName);
        bool isMoving = (moveFloat >= deadzone || moveFloat <= -deadzone);

        if(!isMoving)
        {
            firstMoveInput = false;
            delayFlag = false;
            timer = 0;
            if (!idleFlag)
            {
                OnIdleAxis.Invoke();
                idleFlag = true;
                return;
            }
        }
        else
        {
            idleFlag = false;

            if(!firstMoveInput)
            {
                firstMoveInput = true;
                InvokeMovement(moveFloat);
                return;
            }

            timer += Time.unscaledDeltaTime;
            bool actionTimePassed = (timer >= inputActionsDuration);
            bool delayTimePassed = (timer >= inputDelay);

            if(!delayFlag)
            {
                if(delayTimePassed)
                {
                    timer = 0;
                    delayFlag = true;
                    InvokeMovement(moveFloat);
                }
            }
            else
            {
                if(actionTimePassed)
                {
                    timer = 0;
                    InvokeMovement(moveFloat);
                }
            }
        }
    }

    void InvokeMovement(float moveFloat)
    {
        if(moveFloat > 0) OnPositiveAxis.Invoke();
        else OnNegativeAxis.Invoke();
    }
}
