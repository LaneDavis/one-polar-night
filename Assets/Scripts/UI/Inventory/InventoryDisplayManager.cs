﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InventoryDisplayManager : MonoBehaviour
{
    public class ItemSelectedEvent : UnityEvent<InventoryItem> { }
    static public ItemSelectedEvent OnItemSelected = new ItemSelectedEvent();

    static private List<InventoryDisplay> items = new List<InventoryDisplay>();
    static private InventoryItem[] itemList;

    public Image selectIcon;
    static public bool isSelecting = false;
    static public InventoryDisplayManager instance;
    static public InventoryItem selectedItem;
    static private GameObject selectedButton;
    static public int selectedItemIdx;

    static public int currentIndex = 0;

    private void Awake()
    {
        items.AddRange(GetComponentsInChildren<InventoryDisplay>());
        OnItemSelected.AddListener(SelectItem);
        instance = this;
        InventoryToggle.OnToggleClosed.AddListener(DeselectItems);
    }

    private void OnEnable()
    {
        selectIcon.color = Color.clear;
        isSelecting = false;
        SelectItem(null);
        PopulateItems();
    }

    public static void PopulateItems()
    {
        itemList = Inventory.ItemsOwned;
        for(int i = 0; i < items.Count; i++)
        {
            items[i].item = itemList[i];
            items[i].UpdateDisplay();
        }
        DeselectItems();
    }

    public void SelectItem(InventoryItem item)
    {
        if (item == null) return;
        
        if (isSelecting)
        {
            //Attempt to combine instead
            return;
        }

        selectedButton?.GetComponent<InventoryDisplay>().ResetButton();

        selectIcon.color = Color.white;
        selectIcon.sprite = item.Sprite;

        GameObject targetButton = FindInventoryButtons(item);
        UpdateTargetPosition(targetButton);
        isSelecting = true;
        selectedItem = item;
        selectedButton = FindInventoryButtons(item);
        selectedItemIdx = currentIndex;

        Inventory.TrySelectItem(selectedItemIdx);
    }

    static public void DeselectItems()
    {
        instance.selectIcon.color = Color.clear;
        isSelecting = false;
        selectedButton?.GetComponent<InventoryDisplay>().ResetButton();
        selectedButton = null;
        selectedItem = null;
        return;
    }

    public void UpdateTargetPosition(GameObject button)
    {
        currentIndex = FindButtonIndex(button.GetComponent<InventoryDisplay>());

        selectIcon.GetComponent<TransformFollow>().targetTransform = button.transform;
        if (!isSelecting) selectIcon.GetComponent<TransformFollow>().QuickSnap();
    }

    static int FindButtonIndex(InventoryDisplay button)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == button) return i;
        }

        return -1;
    }

    static GameObject FindInventoryButtons(InventoryItem item)
    {
        for(int i = 0; i < items.Count; i++)
        {
            if (items[i].item == item) return items[i].gameObject;
        }

        return null;
    }

    static public void MoveItem(InventoryDisplay item)
    {
        GameObject currentItem = FindInventoryButtons(selectedItem);
        currentItem.GetComponent<InventoryDisplay>().item = null;
        item.item = selectedItem;
        item.UpdateDisplay();
        currentItem.GetComponent<InventoryDisplay>().UpdateDisplay();


        instance.selectIcon.color = Color.clear;
        isSelecting = false;
    }
}
