﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InventoryToggle : MonoBehaviour
{
    private Button button;
    public Animator anim;

    public Button firstItem;

    static public bool isOpen = false;

    static public UnityEvent OnToggleClosed = new UnityEvent();

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ToggleInventory);
    }

    public void ToggleInventory()
    {
        bool isOpen = !anim.GetBool("isOpen");
        anim.SetBool("isOpen", isOpen);

        if (isOpen) Invoke("SelectItem", .6f);
        InventoryToggle.isOpen = isOpen;
        if(!isOpen) OnToggleClosed.Invoke();
    }

    void SelectItem()
    {
        firstItem.Select();
    }
}
