﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class InventoryDisplay : MonoBehaviour, ISelectHandler, ISubmitHandler, IPointerClickHandler, IPointerEnterHandler
{
    public Image itemObject;
    public GameObject infoObject;
    public TextMeshProUGUI nameObject;
    public TextMeshProUGUI descriptionObject;

    public InventoryItem item;
    
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();

        //InventoryDisplayManager.OnItemSelected.AddListener(ResetItemSelect);
    }

    private void OnEnable()
    {
        UpdateDisplay();
    }

    public void UpdateDisplay()
    {
        if(item == null)
        {
            infoObject.SetActive(false);
            itemObject.color = Color.clear;
            return;
        }
        itemObject.color = Color.white;
        itemObject.sprite = item.Sprite;

        nameObject.text = item.DisplayName;
        descriptionObject.text = item.Description;
        infoObject.SetActive(true);
    }

    void IsSelected()
    {
        if(item == null)
        {
            Inventory.DropItem(InventoryDisplayManager.selectedItemIdx, InventoryDisplayManager.currentIndex);
            InventoryDisplayManager.MoveItem(this);
            InventoryDisplayManager.DeselectItems();
        }
        else if(InventoryDisplayManager.selectedItem == item)
        {
            InventoryDisplayManager.DeselectItems();
            //Inventory.DropItem(InventoryDisplayManager.selectedItemIdx, InventoryDisplayManager.currentIndex);
        }
        else if (InventoryDisplayManager.isSelecting)
        {
            Inventory.DropItem(InventoryDisplayManager.selectedItemIdx, InventoryDisplayManager.currentIndex);
        }
        else {
            Inventory.TrySelectItem(InventoryDisplayManager.currentIndex);
            anim.SetBool("isSelected", true);
            infoObject.SetActive(false);
            InventoryDisplayManager.OnItemSelected.Invoke(item);
        }
        
    }

    public void ResetButton(InventoryItem newItem)
    {
        if (newItem != item) anim.SetBool("isSelected", false);
    }

    public void ResetButton()
    {
        anim.SetBool("isSelected", false);
    }

    public void OnSelect(BaseEventData eventData)
    {
        InventoryDisplayManager.instance.UpdateTargetPosition(gameObject);
    }

    public void OnSubmit(BaseEventData eventData)
    {
        IsSelected();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        IsSelected();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        InventoryDisplayManager.instance.UpdateTargetPosition(gameObject);
    }
}
