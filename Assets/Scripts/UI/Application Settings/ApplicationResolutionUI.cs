﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationResolutionUI : ApplicationSettingsUI {

    private OptionList options;

    private void Awake()
    {
        options = GetComponent<OptionList>();
        Resolution[] resolutionList = ApplicationSettings.AvailableResolutions;

        options.options.Clear();
        for (int i = 0; i < resolutionList.Length; i++)
        {
            options.options.Add(ResolutionToString(resolutionList[i]));
        }
    }

    private void OnEnable()
    {
        options.Value = ApplicationSettings.GetResolutionIndex();
    }

    public void SetResolution()
    {
        ApplicationSettings.SetResolution(options.Value);
    }

    protected override void OnAppliedSettings()
    {
        SetResolution();
    }

    static public string ResolutionToString(Resolution targetResolution)
    {
        string targetString = targetResolution.width + " x " + targetResolution.height + " " + targetResolution.refreshRate + "Hz";
        return targetString;
    }
}
