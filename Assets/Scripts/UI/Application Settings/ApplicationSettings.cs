﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ApplicationSettings : MonoBehaviour {

    //[Header("Default Settings")]
    //public bool defaultFullscreen;
    //public bool defaultVsyncEnabled;

    static private Resolution[] availableResolutions;
    static private bool isFullscreen;
    static private bool vsyncEnabled;
    static private string[] availableQualityLevels;
    static private int currentQualityLevel;
    static private int currentResolutionIndex;

    static public UnityEvent OnAppliedSettings = new UnityEvent();

    public static Resolution[] AvailableResolutions
    {
        get
        {
            return availableResolutions;
        }
    }

    public static string[] AvailableQualityLevels
    {
        get
        {
            return availableQualityLevels;
        }
    }

    public static bool IsFullscreen
    {
        get
        {
            return isFullscreen;
        }
    }

    public static bool VsyncEnabled
    {
        get
        {
            return vsyncEnabled;
        }
    }

    private void Awake()
    {
        availableResolutions = Screen.resolutions;
        isFullscreen = Screen.fullScreen;
        availableQualityLevels = QualitySettings.names;
        currentQualityLevel = QualitySettings.GetQualityLevel();
        QualitySettings.vSyncCount = PlayerPrefs.GetInt("Vsync", 1);

        CheckVSyncEnable();
        SetCurrentResolutionIndex();
    }

    static public void SetQualityLevel(int qualityLevel)
    {
        QualitySettings.SetQualityLevel(qualityLevel);
        currentQualityLevel = qualityLevel;
    }

    static public int GetQualityLevel()
    {
        return currentQualityLevel;
    }

    static public void SetResolution(int targetResolution)
    {
        Resolution target = availableResolutions[targetResolution];
        Screen.SetResolution(target.width, target.height, isFullscreen);
        currentResolutionIndex = targetResolution;
        
    }

    static public int GetResolutionIndex()
    {
        return currentResolutionIndex;
    }

    static public void SetFullScreen(bool value)
    {
        Screen.fullScreen = value;
        isFullscreen = value;
    }

    static public void SetVSyncOn(bool value)
    {
        if (value) QualitySettings.vSyncCount = 1;
        else QualitySettings.vSyncCount = 0;

        vsyncEnabled = value;
        PlayerPrefs.SetInt("Vsync", QualitySettings.vSyncCount);
    }

    void SetCurrentResolutionIndex()
    {
        for(int i = 0; i < availableResolutions.Length; i++)
        {
            if(availableResolutions[i].Equals(Screen.currentResolution))
            {
                currentResolutionIndex = i;
                return;
            }
        }
    }

    void CheckVSyncEnable()
    {
        if (QualitySettings.vSyncCount > 0) vsyncEnabled = true;
        else vsyncEnabled = false;
    }
}
