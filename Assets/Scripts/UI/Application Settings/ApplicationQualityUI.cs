﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationQualityUI : ApplicationSettingsUI {

    private OptionList options;

    private void Awake()
    {
        options = GetComponent<OptionList>();
        string[] qualityList = ApplicationSettings.AvailableQualityLevels;
        options.options.Clear();
        options.options.AddRange(qualityList);
    }

    private void OnEnable()
    {
        options.Value = ApplicationSettings.GetQualityLevel();
    }
  
    public void SetQuality()
    {
        ApplicationSettings.SetQualityLevel(options.Value);
    }

    protected override void OnAppliedSettings()
    {
        SetQuality();
    }
}
