﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationSettingsUI : MonoBehaviour {

    protected void Start()
    {
        ApplicationSettings.OnAppliedSettings.AddListener(OnAppliedSettings);
    }

    protected virtual void OnAppliedSettings()
    {

    }
}
