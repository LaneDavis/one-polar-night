﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApplicationFullscreenUI : ApplicationSettingsUI {

    private Toggle currentToggle;

    private void Awake()
    {
        currentToggle = GetComponent<Toggle>();
    }

    private void OnEnable()
    {
        currentToggle.isOn = ApplicationSettings.IsFullscreen;
    }

    protected override void OnAppliedSettings()
    {
        ApplicationSettings.SetFullScreen(currentToggle.isOn);
    }
}
