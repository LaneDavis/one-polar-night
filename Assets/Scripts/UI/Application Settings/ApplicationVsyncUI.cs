﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApplicationVsyncUI : ApplicationSettingsUI {

    private Toggle currentToggle;

    private void Awake()
    {
        currentToggle = GetComponent<Toggle>();
    }

    private void OnEnable()
    {
        currentToggle.isOn = ApplicationSettings.VsyncEnabled;
    }

    protected override void OnAppliedSettings()
    {
        ApplicationSettings.SetVSyncOn(currentToggle.isOn);
    }
}
