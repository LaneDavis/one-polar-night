﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInitialize : MonoBehaviour
{
    public CharacterManager manager;

    private void Awake()
    {
        CharacterManager.instance = manager;
    }
}
