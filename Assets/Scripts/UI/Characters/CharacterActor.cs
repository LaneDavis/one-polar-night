﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterActor
{
    public Character characterEnum;
    public Sprite neutralSprite;
    public Sprite happySprite;
    public Sprite sadSprite;
    public AudioClip neutralSound;
    public AudioClip happySound;
    public AudioClip sadSound;

    public string GetName()
    {
        return characterEnum.ToString();
    }

    public Sprite GetSprite(Emotion emotion)
    {
        Sprite targetSprite;
        switch (emotion)
        {
            case Emotion.Neutral:
                targetSprite = neutralSprite;
                break;
            case Emotion.Happy:
                targetSprite = happySprite;
                break;
            case Emotion.Sad:
                targetSprite = sadSprite;
                break;
            default:
                targetSprite = neutralSprite;
                break;
        }

        return targetSprite;
    }

    public AudioClip GetAudioClip(Emotion emotion)
    {
        AudioClip clip;
        switch (emotion)
        {
            case Emotion.Neutral:
                clip = neutralSound;
                break;
            case Emotion.Happy:
                clip = happySound;
                break;
            case Emotion.Sad:
                clip = sadSound;
                break;
            default:
                clip = neutralSound;
                break;
        }

        return clip;
    }
}
