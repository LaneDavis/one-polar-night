﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Character { None = -1, Player, Dansgaard, Zelda, Steve, Richard }
public enum Emotion { Neutral, Happy, Sad }

[CreateAssetMenu(fileName = "[Character Manager]", menuName = "Character Manager")]
public class CharacterManager : ScriptableObject
{
    public List<CharacterActor> characters;
    static public CharacterManager instance;

    static public CharacterActor GetCharacter(Character character)
    {
        for(int i = 0; i < instance.characters.Count; i++)
        {
            if (instance.characters[i].characterEnum == character) return instance.characters[i];
        }

        Debug.LogError("Character doesn't exist: " + character.ToString());
        return null;
    }
}
