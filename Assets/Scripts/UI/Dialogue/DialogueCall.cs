﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueCall : MonoBehaviour
{
    public DialogueObject dialogue;
    private Button button;
    public Text text;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(CallDialogue);
        text.text = dialogue.name;
    }

    void CallDialogue()
    {
        DialogueDisplay.DisplayDialogue(dialogue);
    }
}
