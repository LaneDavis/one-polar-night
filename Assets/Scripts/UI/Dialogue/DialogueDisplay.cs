﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class DialogueDisplay : MonoBehaviour
{
    public TextMeshProUGUI speakerText;
    public TextMeshProUGUI dialogueText;

    public Image[] positions;

    public Color dimColor = Color.gray;

    static private DialogueObject dialogue;
    static private int currentLine = 0;

    public float talkingOffsetAmount = 0;
    public Animator anim;

    static public bool isDisplayed = false;

    private AudioSource audio;

    private AudioClip previousClip;

    // Delay between dialogs
    public static float lastDialogClosedTime = -10f;
    private const float timeBetweenDialogs = 0.5f;
    public static bool TooSoonToInitiateNewDialog => Time.time < lastDialogClosedTime + timeBetweenDialogs;
    
    

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    static public void DisplayDialogue(DialogueObject newDialogue)
    {
        dialogue = newDialogue;
        currentLine = 0;
        isDisplayed = true;
        MenuSystemManager.instance.SwitchMenu("Dialogue");
    }

    private void OnEnable()
    {
        currentLine = 0;
        ShowActors();
        UpdateDisplay();
    }

    public void NextDialogue()
    {
        if(currentLine + 1 >= dialogue.lines.Length)
        {
            EndDialogue();
            return;
        }

        currentLine++;
        
        UpdateDisplay();
    }

    void UpdateDisplay()
    {
        CharacterActor speaker = CharacterManager.GetCharacter(dialogue.lines[currentLine].speakingCharacter);

        dialogueText.text = dialogue.lines[currentLine].dialogue;
        speakerText.text = speaker.GetName();

        for(int i = 0; i < positions.Length; i++)
        {
            if(i > dialogue.actors.Length - 1)
            {
                positions[i].gameObject.SetActive(false);
            }
            else
            {
                positions[i].gameObject.SetActive(true);
                CharacterActor actor = CharacterManager.GetCharacter(dialogue.actors[i]);

                if (actor == speaker) 
                {
                    positions[i].sprite = actor.GetSprite(dialogue.lines[currentLine].emotion);
                    positions[i].color = Color.white;
                    anim.SetInteger("ActorNumber", i+1);
                    AudioClip clip = actor.GetAudioClip(dialogue.lines[currentLine].emotion);
                    if (previousClip != clip)
                    {
                        audio.clip = clip;
                        audio.Play();
                        previousClip = clip;
                    }
                }
                else positions[i].color = dimColor;
            }
        }
    }

    void ShowActors()
    {
        for(int i = 0; i < positions.Length; i++)
        {
            if (i > dialogue.actors.Length - 1)
            {
                positions[i].gameObject.SetActive(false);
            }
            else
            {
                positions[i].gameObject.SetActive(true);
                CharacterActor actor = CharacterManager.GetCharacter(dialogue.actors[i]);

                positions[i].sprite = actor.GetSprite(Emotion.Neutral);
            }
        }
    }

    void EndDialogue()
    {
        dialogue = null;
        isDisplayed = false;
        MenuSystemManager.instance.LastOpenedMenu();
        lastDialogClosedTime = Time.time;
    }
}
