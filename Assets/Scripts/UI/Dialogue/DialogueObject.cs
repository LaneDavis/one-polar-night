﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue Object", menuName = "Dialogue")]
public class DialogueObject : ScriptableObject
{
    public Character[] actors = new Character[2];

    public DialogueLine[] lines;
}