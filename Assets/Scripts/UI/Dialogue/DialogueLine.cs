﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueLine
{
    public Character speakingCharacter;
    public Emotion emotion;
    [TextArea()] public string dialogue;
}
