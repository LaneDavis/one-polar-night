﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

    private Camera mainCamera;
    public bool onlyHorizontal = false;

    private void Start() {
        mainCamera = Camera.main;
    }

    private void Update() {
        transform.LookAt(mainCamera.transform);
        if (onlyHorizontal) {
            float y = transform.rotation.eulerAngles.y;
            transform.rotation = Quaternion.identity;
            transform.Rotate(0f, y, 0f);
        }
    }

}
