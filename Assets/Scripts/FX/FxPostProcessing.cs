﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxPostProcessing : FxPackage
{
    //FACTOR ATTRIBUTES
    public PPFactor Factor;
    
    private void Update () {

        //If toggled on, set the shake value based on the curve.
        if (ToggleState) {
//			MenuScreenshakeManager.Instance.SetScreenshake(SfxId, ValueCurve.Evaluate(Controller.Timer) * ValueMultiplier, DecayRate);
        }

    }
    
    // Use for steady-state factors
    public override void Toggle (bool toggleState, GameObject newAnchor = null) {

        base.Toggle(toggleState, newAnchor);
        
        if (!ToggleState && toggleState) {
            PPManager.Instance.AddFactor(Factor);
        }

        if (ToggleState && !toggleState) {
            PPManager.Instance.RemoveFactor(Factor);
        }

        ToggleState = toggleState;
        if (newAnchor != null)
            Anchor = newAnchor;
		
    }

    public override void Trigger (GameObject newAnchor = null) {
        base.Trigger(newAnchor);
        PPManager.Instance.AddFactor(Factor);
    }
    
}
