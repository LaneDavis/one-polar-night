﻿

using UnityEngine;

public class FxScreenshake : FxPackage {

	//FACTOR ATTRIBUTES
	[Range(0F, 1f)] public float ValueMultiplier = .3f;
	
	private void Update () {

		//If toggled on, set the shake value based on the curve.
		if (ToggleState) {
//			MenuScreenshakeManager.Instance.SetScreenshake(SfxId, ValueCurve.Evaluate(Controller.Timer) * ValueMultiplier, DecayRate);
		}

	}

	public override void Trigger (GameObject newAnchor = null) {
		base.Trigger(newAnchor);
		Camshake.Instance.AddTrauma(ValueMultiplier);
	}

}
