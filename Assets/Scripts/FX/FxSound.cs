﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxSound : FxPackage {

    public SoundCall instantSound;
    public SoundCall durationLoop;
    protected float baseDurationVolume = 0F;

    public AnimationCurve durationCurve = AnimationCurve.EaseInOut(0F, 0F, 1F, 1F);
    public float durationDecaySpeed = 5F;

    //State: Time
    protected float durationTimer;
    //State: Strength
    protected float curDurationMultiplier;

    public override void SetUp (GameObject newAnchor = null) {

        baseDurationVolume = durationLoop.volume;
        durationLoop.volume = 0F;
        
    }
    
    public override void Trigger (GameObject newAnchor = null) {
        SoundManager.instance.PlaySound(instantSound, SoundManager.thisSoundManager.gameObject);
    }
    
    void Update () {

        // Do Nothing

    }

}