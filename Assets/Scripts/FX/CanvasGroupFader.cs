﻿using System;
using UnityEngine;

/// <summary>
/// Handles fading a canvas group in or out
/// </summary>
public class CanvasGroupFader : MonoBehaviour {



	public float Speed = 2f;
	private float direction;
	private bool doFade;

	public CanvasGroup Target;

	private Action callback;


	public void AllIn() {
		doFade = false;
		Target.alpha = 1;
		callback = null;
	}

	public void AllOut() {
		doFade = false;
		Target.alpha = 0;
		callback = null;
	}

	public void FadeIn(Action whenComplete=null) {
		DoFade(1, whenComplete);
	}

	public void FadeOut(Action whenComplete=null) {
		DoFade(-1, whenComplete);
	}

	private void DoFade(float direc, Action whenComplete) {
		direction = direc;
		doFade = true;
		callback = whenComplete;
	}

	private void Update() {
		if (!doFade) return;

		float fade = Target.alpha + direction * Speed * Time.deltaTime;
		Target.alpha = fade;
		if (direction < 0 && fade <= 0) {
			doFade = false;
		}
		else if (direction > 0 && fade >= 1) {
			doFade = false;
		}

		if (!doFade && callback != null) {
			callback();
			callback = null;
		}
	}
}
