﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxRumble : FxPackage {
    
    //FACTOR ATTRIBUTES
    [Range(0F, 1f)] public float ValueMultiplier = .3f;
	
    private void Update () {

        //If toggled on, set the shake value based on the curve.
        if (ToggleState) {
			RumbleManager.Instance.AddTrauma(ValueMultiplier);
        }

    }

    public override void Trigger (GameObject newAnchor = null) {
        base.Trigger(newAnchor);
        RumbleManager.Instance.AddTrauma(ValueMultiplier);
    }
    
}
