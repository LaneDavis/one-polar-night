﻿using UnityEngine;

public class FxShakeObject : FxPackage {

	//FACTOR ATTRIBUTES
	[Range(0F, 15f)] public float DecayRate = 3f;
	[Range(0F, 100f)] public float ValueMultiplier = 5f;
	public float PerlinSpeed = 10f;
	public AnimationCurve ValueCurve = new AnimationCurve (new Keyframe (0f, 1f), new Keyframe (1f, 1f)); 

	//ANATOMY
	private FxApplierTranslate applier;

	public override void SetUp (GameObject newAnchor = null) {
		
		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;

		if (newAnchor.GetComponent<FxApplierTranslate>() != null) {
			applier = newAnchor.GetComponent<FxApplierTranslate>();
		} else {
			applier = newAnchor.AddComponent<FxApplierTranslate>();
		}
		applier.SetUp(newAnchor);

	}

	private void Update () {

		//If toggled on, set the shake value based on the curve.
		if (ToggleState) {
			applier.SetShakeFactor(SfxId, ValueCurve.Evaluate(Controller.Timer) * ValueMultiplier, PerlinSpeed, DecayRate);
		}

	}

	public override void Trigger (GameObject newAnchor = null) {
		base.Trigger(newAnchor);
		applier.SetShakeFactor(SfxId, ValueCurve.Evaluate(0F) * ValueMultiplier, PerlinSpeed, DecayRate);
	}

}
