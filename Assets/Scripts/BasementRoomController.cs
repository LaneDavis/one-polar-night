﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasementRoomController : MonoBehaviour
{
    public static BasementRoomController Instance;

    public Transform basementRoomTransform;
    
    public Transform inTransform;
    public Transform outTransform;

    public AnimationCurve xCurve;
    public AnimationCurve yCurve;
    public AnimationCurve rotCurve;

    private bool isOpening;
    public float OpenTime;
    private float timer;
    public float DecayRate = 8f;

    private float Progress => Mathf.Clamp01(timer / OpenTime);
    private float curProgress = 0f;
    
    private void Awake() {
        Instance = this;
    }

    private void Update() {

        // Update desired position
        timer = Mathf.Clamp(timer + (isOpening ? Time.deltaTime : - Time.deltaTime * 1.75f), 0f, OpenTime);
        curProgress = MathUtilities.DecayToward(curProgress, Progress, Time.deltaTime, DecayRate);
        
        // Move to desired position
        basementRoomTransform.rotation = Quaternion.LerpUnclamped(inTransform.rotation, outTransform.rotation, rotCurve.Evaluate(curProgress));
        float x = Mathf.LerpUnclamped(inTransform.position.x, outTransform.position.x, xCurve.Evaluate(curProgress));
        float y = Mathf.LerpUnclamped(inTransform.position.y, outTransform.position.y, yCurve.Evaluate(curProgress));
        float z = Mathf.LerpUnclamped(inTransform.position.z, outTransform.position.z, xCurve.Evaluate(curProgress));
        basementRoomTransform.position = new Vector3(x, y, z);


    }

    public void StartOpening() {
        isOpening = true;
    }

    public void StartClosing() {
        isOpening = false;
    }
    
}
