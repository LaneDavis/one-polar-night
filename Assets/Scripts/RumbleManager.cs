﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class RumbleManager : MonoBehaviour {

    public static RumbleManager Instance;
    private Player player;
    
    public float DecayRate = 8f;

    private float curTrauma;
    
    private void Awake() {
        Instance = this;
    }

    private void Start() {
        player = ReInput.players.GetPlayer(0);
    }

    public void AddTrauma(float trauma) {
        curTrauma = Mathf.Max(curTrauma, trauma);
    }

    private void Update() {
        curTrauma = MathUtilities.Decay(curTrauma, Time.deltaTime, DecayRate);
        player.SetVibration(0, curTrauma);
        player.SetVibration(1, curTrauma);
    }

}
