﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class LaboratoryController : MonoBehaviour {

    public static LaboratoryController Instance;

    public Transform upstairsTransform;
    
    public Transform closedTransform;
    public Transform openTransform;

    public AnimationCurve xCurve;
    public AnimationCurve yCurve;
    public AnimationCurve rotCurve;

    private bool isOpening;
    public float OpenTime;
    private float timer;
    public float DecayRate = 8f;

    private float Progress => Mathf.Clamp01(timer / OpenTime);
    private float curProgress = 0f;
    
    private void Awake() {
        Instance = this;
    }

    private void Update() {

        // Update desired position
        timer = Mathf.Clamp(timer + (isOpening ? Time.deltaTime : - Time.deltaTime * 1.75f), 0f, OpenTime);
        curProgress = MathUtilities.DecayToward(curProgress, Progress, Time.deltaTime, DecayRate);
        
        // Move to desired position
        upstairsTransform.rotation = Quaternion.LerpUnclamped(closedTransform.rotation, openTransform.rotation, rotCurve.Evaluate(curProgress));
        float x = Mathf.LerpUnclamped(closedTransform.position.x, openTransform.position.x, xCurve.Evaluate(curProgress));
        float y = Mathf.LerpUnclamped(closedTransform.position.y, openTransform.position.y, yCurve.Evaluate(curProgress));
        float z = Mathf.LerpUnclamped(closedTransform.position.z, openTransform.position.z, xCurve.Evaluate(curProgress));
        upstairsTransform.position = new Vector3(x, y, z);


    }

    public void StartOpening() {
        isOpening = true;
    }

    public void StartClosing() {
        isOpening = false;
    }
    

}
