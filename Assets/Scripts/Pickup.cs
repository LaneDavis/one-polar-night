﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public InventoryItem Item;
    public Animator Animator;
    public FxPackageController FxCollect;
    public ParticleSystem SteadyParticles;

    private SpriteRenderer SpriteRenderer;

    private bool hasBeenPickedUp;

    private void Start() {
        SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        SpriteRenderer.sprite = Item.Sprite;
    }
    
    private void DoPickup(Collider other) {
        if (hasBeenPickedUp) return;
        if (!Inventory.PickupItem(Item)) return;
        hasBeenPickedUp = true;
        Animator.Play("Pickup Collect");
        SteadyParticles.Stop();
        FxCollect.TriggerAll();
        other.GetComponent<CharacterAnimations>().DoPickupAnimation();
    }

    public void KillSelf() {
        Destroy(gameObject);
    }
    
    private void OnTriggerEnter(Collider other) {
        Debug.Log("OnTrigger");
        if (other.gameObject.tag == "Player") {
            DoPickup(other);
        }
    }

    private void OnCollisionEnter(Collision other) {
        Debug.Log("OnCollision");
        if (other.gameObject.tag == "Player") {
            DoPickup(other.collider);
        }   
    }
    
}
