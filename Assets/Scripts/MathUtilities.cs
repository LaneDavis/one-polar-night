﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathUtilities : MonoBehaviour {

	public static float degreesToLookAlongVector(Vector2 vec) {
		float deg = Mathf.Atan(vec.x / vec.y) * Mathf.Rad2Deg;
		if (vec.y > 0) {
			deg = -deg + 90;
		} else {
			deg = -deg + 270;
		}
		return deg;

	}
	
	/// <summary>
	/// Decay the specified value over a given time at a given speed.
	/// Note that while time will most commonly be seconds, it doesn't
	/// matter as long as t and s use the same unit for time.
	/// </summary>
	/// <returns>A value somewhat closer to 0.</returns>
	/// <param name="v">The value being decayed.</param>
	/// <param name="t">The time during which the decay takes place.</param>
	/// <param name="s">The rate of decay per unit time.</param>
	public static float Decay(float v, float t, float s) {
		return v / Mathf.Exp(t * s);
	}
		
	/// <summary>
	/// Decay the specified value toward a target value.
	/// </summary>
	/// <returns>A calue somewhat closer to the targetValue.</returns>
	/// <param name="value">The value being decayed.</param>
	/// <param name="targetValue">The target aproached by the value.</param>
	/// <param name="t">The time during which the decay takes place.</param>
	/// <param name="s">The rate of decay per unit time.</param>
	public static float DecayToward(float value, float targetValue, float t, float s) {
		return targetValue - Decay(targetValue - value, t, s);
	}
	
}
