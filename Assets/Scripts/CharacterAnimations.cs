﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimations : MonoBehaviour {
    [Header("Walking")]
    public FxPackageController FxLeftFast;
    public FxPackageController FxLeftSlow;
    public FxPackageController FxRightFast;
    public FxPackageController FxRightSlow;

    public float fastThreshold;
    public float movePerPulse;
    private float unspentMove;
    private bool nextIsRight = true;

    public GameObject SpriteFlipObject;
    private Vector3 spriteFlipBaseScale;
    private bool facingRight;

    public float stepRumble;
    public SoundCall stepSound;
    
    [Header("Pickup")]
    public FxPackageController FxPickup;

    private void Awake() {
        spriteFlipBaseScale = SpriteFlipObject.transform.localScale;
    }
    
    public void WalkAnimation(float horizontalMove) {
        unspentMove += Mathf.Abs(horizontalMove) * Time.deltaTime;
        if (unspentMove > movePerPulse) {
            unspentMove = 0f;
            if (nextIsRight) {
                if (Mathf.Abs(horizontalMove * Time.deltaTime) > fastThreshold) {
                    FxRightFast.TriggerAll();
                    RumbleManager.Instance.AddTrauma(stepRumble);
                    if (stepSound != null) SoundManager.instance.PlaySound(stepSound, gameObject);
                } else {
                    FxRightSlow.TriggerAll();
                    RumbleManager.Instance.AddTrauma(stepRumble);
                    if (stepSound != null) SoundManager.instance.PlaySound(stepSound, gameObject);
                }
            } else {
                if (Mathf.Abs(horizontalMove * Time.deltaTime) > fastThreshold) {
                    FxLeftFast.TriggerAll();
                    RumbleManager.Instance.AddTrauma(stepRumble);
                    if (stepSound != null) SoundManager.instance.PlaySound(stepSound, gameObject);
                } else {
                    FxLeftSlow.TriggerAll();
                    RumbleManager.Instance.AddTrauma(stepRumble);
                    if (stepSound != null) SoundManager.instance.PlaySound(stepSound, gameObject);
                }
            }

            nextIsRight = !nextIsRight;
        }
    }

    public void UpdateFacing(float xVal) {
        if (!facingRight && xVal > 0f) {
            facingRight = true;
            SpriteFlipObject.transform.localScale = new Vector3(-spriteFlipBaseScale.x, spriteFlipBaseScale.y, spriteFlipBaseScale.z);
        } else if (facingRight && xVal < 0f) {
            facingRight = false;
            SpriteFlipObject.transform.localScale = new Vector3(spriteFlipBaseScale.x, spriteFlipBaseScale.y, spriteFlipBaseScale.z);
        }
    }

    public void DoPickupAnimation() {
        FxPickup.TriggerAll();
    }
    
}
