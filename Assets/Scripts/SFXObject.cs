﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXObject : MonoBehaviour {

    public float Duration;
    public float DeletionTime;
	
    protected float timer;
    protected float deletionTimer;
    protected bool isOn;

    public bool TriggerOnStart = true;
	
    protected float Progress {
        get { return timer / Duration; }
    }

    protected void Start() {
        if (TriggerOnStart) {
            StartFX();
        }
    }
	
    public virtual void StartFX() {
		
        foreach (FxPackageController fx in GetComponentsInChildren<FxPackageController>()) {
            fx.TriggerAll();
        }

        timer = 0f;
        isOn = true;
		
    }
	
    public virtual void Update() {
		
        if (isOn) {
            timer += Time.deltaTime;
            if (Progress >= 1f) {
                Stop();
            }
        }

        if (!isOn) {
            deletionTimer += Time.deltaTime;
            if (deletionTimer > DeletionTime) {
                DestroySelf();
            }
        }
    }
	
    protected virtual void Stop() {
        isOn = false;
        timer = 0f;
    }

    protected virtual void DestroySelf() { Destroy(gameObject); }
	
}
