﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayer : MonoBehaviour {

	public static ChasePlayer Instance;
	
	public AnimationCurve ChaseCurve;

	public GameObject playerObject;
	public Vector3 Offset = new Vector3(0f, 2.2f, -6.35f);
	public Vector3 DialogueOffset = new Vector3(0f, 2.2f, -6.35f);

	private void Awake() { Instance = this;}

	private void Start() {
		SnapToPlayer();
	}
	
	private void Update() {
		Chase();
	}
	
	public void SnapToPlayer() {
		Vector3 targetPosition = playerObject.transform.position + Offset;
		transform.position = targetPosition;
	}

	private void Chase() {
		Vector3 targetPosition = Vector3.zero;
		if (DialogueDisplay.isDisplayed) {
			targetPosition = playerObject.transform.position + DialogueOffset;
		}
		else {
			targetPosition = playerObject.transform.position + Offset;
		}
		float dist = Vector3.Distance(targetPosition, transform.position);
		Vector3 dir = (targetPosition - transform.position).normalized;
		transform.Translate(dir * ChaseCurve.Evaluate(dist) * Time.deltaTime, Space.World);
		
	}
}
