﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class NPCManager : MonoBehaviour {

    public static NPCManager Instance;
    public List<NPCController> NPCs = new List<NPCController>();
    public float DialogueActivationDistance = 2.5f;

    [Header("Ending")]
    public DialogueObject EndingDialogue;
    public CanvasGroup EndingShade;
    public AnimationCurve EndingShadeOpac;
    public float EndingRepositionDelay;
    public float EndingDialogueDelay;
    public List<Transform> EndingNPCTransforms = new List<Transform>();
    public Transform EndingPlayerTransform;

    private bool endingTriggered;
    private bool endingHasDoneReposition;
    private bool endingHasDoneDialogueStart;
    private float endingTimer;

    public bool FakeTriggerEnding;
    
    private void Awake() {
        Instance = this;
    }

    public NPCController NearbyNPC(Transform playerTransform) {
        NPCController bestNPCSoFar = null;
        float bestDistSoFar = Mathf.Infinity;
        foreach (NPCController NPC in NPCs) {
            float dist = Vector3.Distance(NPC.transform.position, playerTransform.position);
            if (dist < Mathf.Min(DialogueActivationDistance, bestDistSoFar)) {
                bestDistSoFar = dist;
                bestNPCSoFar = NPC;
            }
        }
        return bestNPCSoFar;
    }

    private void Update() {

        if (DialogueDisplay.isDisplayed) return;

        if (FakeTriggerEnding) {
            FakeTriggerEnding = false;
            endingTriggered = true;
        }
        
        if (!endingTriggered) {
            foreach (NPCController npc in NPCs) {
                if (!npc.hasReceivedDesiredItem) return;
            }

            endingTriggered = true;
        }

        else {

            endingTimer += Time.deltaTime;
            EndingShade.alpha = EndingShadeOpac.Evaluate(endingTimer);
            if (!endingHasDoneReposition && endingTimer > EndingRepositionDelay) {
                for (int i = 0; i < NPCs.Count; i++) {
                    NPCs[i].transform.position = EndingNPCTransforms[i].position;
                }

                PlayerController.Instance.transform.position = EndingPlayerTransform.position;
                
                endingHasDoneReposition = true;
            }

            if (!endingHasDoneDialogueStart && endingTimer > EndingDialogueDelay) {
                DialogueDisplay.DisplayDialogue(EndingDialogue);
                endingHasDoneDialogueStart = true;
            }

        }
        
    }
    
}
