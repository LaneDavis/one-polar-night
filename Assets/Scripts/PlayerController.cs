﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Player player;
    
    public static PlayerController Instance;
    public GameObject CombineSFXPrefab;
    
    private void Awake() {
        Instance = this;
    }

    private void Start () {
        player = ReInput.players.GetPlayer(0);
    }
    
    public void Update() {
        NPCController nearbyNPC = NPCManager.Instance.NearbyNPC(transform);
        if (player.GetButtonDown("Interact") && nearbyNPC != null) {
            if (DialogueDisplay.isDisplayed) return;
            if (DialogueDisplay.TooSoonToInitiateNewDialog) return;
            if (InventoryToggle.isOpen) return;
            nearbyNPC.Talk();
        }
    }
    
    public void DoCombineSFX() {
        Instantiate(CombineSFXPrefab, transform.position, Quaternion.identity, null);
    }

}
