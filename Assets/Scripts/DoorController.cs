﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {

    public Animator Animator;
    public SoundCall OpenSound;
    public SoundCall CloseSound;
    public float DistanceTrigger;

    private enum State {
        closed,
        opening,
        open,
        closing
    }

    private State state = State.closed;

    private bool PlayerInTrigger => Vector3.Distance(transform.position, PlayerController.Instance.transform.position) < DistanceTrigger;

    private void Update() {

        if (state == State.closed && PlayerInTrigger) {
            StartOpening();
        }

        if (state == State.open && !PlayerInTrigger) {
            StartClosing();
        }
        
    }

    private void StartOpening() {
        state = State.opening;
        Animator.Play("Door Opening");
        SoundManager.instance.PlaySound(OpenSound, gameObject);
    }

    private void StartClosing() {
        state = State.closing;
        Animator.Play("Door Closing");
        SoundManager.instance.PlaySound(CloseSound, gameObject);
    }

    public void ReportOpeningFinished() {
        state = State.open;
    }

    private void ReportClosingFinished() {
        state = State.closed;
    }

}
