﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public static class Inventory {

    public const int InventoryMax = 12;

    public static InventoryItem[] ItemsOwned = new InventoryItem[InventoryMax];
    public static int selectedSlot = -1;

    private static ItemDatabase itemData;

    public static int NextOpenSlot { get {
            for (int i = 0; i < InventoryMax; i++) {
                if (ItemsOwned[i] == null) return i;
            }
            return -1;
    } }

    public static void Init(ItemDatabase itemDatabase) {
        itemData = itemDatabase;
    }

    public static bool PickupItem(InventoryItem newItem) {
        if (NextOpenSlot < 0) return false;
        PutItemInSlot(newItem, NextOpenSlot);
        return true;
    }

    public static bool TrySelectItem(int selectionIdx) {
        if (selectionIdx < 0 || selectionIdx >= InventoryMax || ItemsOwned[selectionIdx] == null) return false;
        selectedSlot = selectionIdx;
        return true;
    }

    private static void PutItemInSlot(InventoryItem item, int slot) {
        if (slot < 0 || slot >= InventoryMax) return;
        
        // TODO: Charles, you can add the appearance of the item sprite in the inventory here. You can grab the sprite the inventory item should use by calling newItem.Sprite.

        ItemsOwned[slot] = item;
    }

    private static void SetSlotToEmpty (int slot) {
        if (slot < 0 || slot >= InventoryMax) return;
        // TODO: Charles, if you want to animate out the item from the slot, you can do that here.
        ItemsOwned[slot] = null;
    }

    /// <summary>
    /// Attempt to combine two items, destroying both and gaining a new item.
    /// The currently selected item is the first one, and the "targetIdx" is the second item onto which you are dropping the first the selected item.
    /// </summary>
    /// <param name="targetSlotIdx">The index of the item onto which you are dropping the selected item.</param>
    /// <returns></returns>
    public static bool DropItem(int selectedSlotIdx, int targetSlotIdx) {
        if (selectedSlotIdx < 0 || ItemsOwned[selectedSlotIdx] == null) return false;
        if (targetSlotIdx < 0 || targetSlotIdx >= InventoryMax) {
            SetSlotToEmpty(selectedSlotIdx);
            return false;
        }
        InventoryItem selectedItem = ItemsOwned[selectedSlotIdx];
        if (ItemsOwned[targetSlotIdx] == null) {            // Drop into empty slot. Move the item over.
            SetSlotToEmpty(selectedSlotIdx);
            PutItemInSlot(selectedItem, targetSlotIdx);
            return false; 
        } else {                                              // Drop into full slot. Attempt to combine items.
            InventoryItem targetedItem = ItemsOwned[targetSlotIdx];
            ItemCombination combination = itemData.CombinationFromItems(selectedItem, targetedItem);
            if (combination == null) {
                // TODO: Charles, if you want to add some kind of "nope" shake when combining, you can do that here.
                selectedSlotIdx = -1;
                return false;
            } else {
                PutItemInSlot(combination.output, targetSlotIdx);
                SetSlotToEmpty(selectedSlotIdx);
                PlayerController.Instance.DoCombineSFX();
                InventoryDisplayManager.PopulateItems();
                return true;
            }
        }
    }
    
}
