﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitialization : MonoBehaviour {

    public ItemDatabase ItemDatabase;
    public PPFactor PPDefault;

    private bool firstUpdate = true;

    private void Awake() {
        Inventory.Init(ItemDatabase);
    }

    private void FirstUpdate() {
        firstUpdate = false;
        PPManager.Instance.AddFactor(PPDefault);
    }

    private void Update() {
        if (firstUpdate) FirstUpdate();
    }
    

}
