﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.UI;

public class InteractButtonDisplayer : MonoBehaviour {
    
    private const float BaseCameraDistance = 15f;

    public RectTransform buttonRT;
    public Image buttonImage;
    
    public AnimationCurve HeightCurve;
    public AnimationCurve OpacityCurve;
    public AnimationCurve ScaleCurve;

    public float DecayRate;

    private bool isOn;
    public float Duration;
    private float timer;

    private float Progress => Mathf.Clamp01(timer / Duration);
    private float curProgress;

    private void Update() {
        
        timer = Mathf.Clamp(timer + (isOn ? Time.deltaTime : -Time.deltaTime * 1.5f), 0f, Duration);
        
        curProgress = MathUtilities.DecayToward(curProgress, Progress, Time.deltaTime, DecayRate);
        buttonRT.anchoredPosition = new Vector2(0f, HeightCurve.Evaluate(curProgress));
        float scale = ScaleCurve.Evaluate(curProgress);
        buttonRT.transform.localScale = new Vector3(scale, scale, scale);
        buttonImage.color = new Color(1f, 1f, 1f, OpacityCurve.Evaluate(curProgress));

        if (Vector3.Distance(transform.position, PlayerController.Instance.transform.position) < NPCManager.Instance.DialogueActivationDistance) {
            isOn = true;
        }
        else {
            isOn = false;
        }
    }
    
}
