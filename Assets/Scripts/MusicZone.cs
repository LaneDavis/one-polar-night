﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicZone : MonoBehaviour {

    public List<MusicFactor> Factors = new List<MusicFactor>();
    public bool StartZone;

    private bool firstUpdate = true;
    
    private void SetMusic() {
        MusicManager.Instance.ClearAllFactors();
        foreach (MusicFactor factor in Factors) {
            MusicManager.Instance.AddFactor(factor);
        }
    }


    private void Update() {
        if (firstUpdate) {
            firstUpdate = false;
            if (StartZone) SetMusic();
        }
    }
    
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            SetMusic();
        }
    }

    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Player") {
            SetMusic();
        }
    }
}
