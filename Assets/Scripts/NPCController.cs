﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NPCController : MonoBehaviour {

    [Header("Dialogue")]
    public InventoryItem DesiredItem;
    [HideInInspector] public bool hasReceivedDesiredItem;
    public List<DialogueObject> PreDialogues = new List<DialogueObject>();
    private int nextPreDialogue;
    public List<DialogueObject> PostDialogues = new List<DialogueObject>();
    private int nextPostDialogue;
    public GameObject SuccessSFXPrefab;
    
    public float triggerDistance = 2.5f;

    [Header("Movement")]
    public AnimationCurve SpeedByDistance;
    private Vector3 targetPosition = Vector3.zero;
    public CharacterAnimations CharacterAnimations;
    private Rigidbody rb;

    private void Start() {
        rb = GetComponent<Rigidbody>();
        targetPosition = transform.position;
    }
    
    public void SetTargetPosition(Vector3 targetPos) {
        targetPosition = targetPos;
    }
    
    public void Talk() {

        if (!hasReceivedDesiredItem) {
            if (Inventory.ItemsOwned.Contains(DesiredItem)) {
                for (int i = 0; i < Inventory.InventoryMax; i++) {
                    if (Inventory.ItemsOwned[i] == DesiredItem) {
                        Inventory.DropItem(i, -1);
                    }
                }
                Instantiate(SuccessSFXPrefab, PlayerController.Instance.transform.position, Quaternion.identity, null);
                hasReceivedDesiredItem = true;
            }
        }

        if (!hasReceivedDesiredItem) {
            RumbleManager.Instance.AddTrauma(0.5f);
            DialogueDisplay.DisplayDialogue(PreDialogues[Mathf.Min(nextPreDialogue, PreDialogues.Count - 1)]);
            nextPreDialogue++;
        } else {
            RumbleManager.Instance.AddTrauma(0.5f);
            DialogueDisplay.DisplayDialogue(PostDialogues[Mathf.Min(nextPostDialogue, PostDialogues.Count - 1)]);
            nextPostDialogue++;
        }
        
    }

    private void Update() {

        Vector3 vec2targ = targetPosition - transform.position;
        Vector3 dir = vec2targ.normalized;
        float dist = vec2targ.magnitude;
        if (dist > 0.25f) {
            Vector3 moveVec = dir * SpeedByDistance.Evaluate(dist);
            Vector3 r = rb.velocity;
            r.x = moveVec.x;
            r.z = moveVec.z;
            rb.velocity = r;
            
            CharacterAnimations.WalkAnimation((new Vector2(r.x, r.z)).magnitude);
            CharacterAnimations.UpdateFacing(r.x);
        }
        
    }

}
