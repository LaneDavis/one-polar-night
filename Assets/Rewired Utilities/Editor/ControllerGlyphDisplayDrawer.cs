﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ControllerGlyphDisplay))]
[CanEditMultipleObjects]
public class ControllerGlyphDisplayDrawer : Editor {

    SerializedProperty propAction;

    SerializedProperty propIcon;
    SerializedProperty propIconAutoFill;

    SerializedProperty propPromptTarget;

    SerializedProperty propAutoCycle;
    SerializedProperty propDuration;

    SerializedProperty propSystemPlayer;
    SerializedProperty propPlayerNumber;

    private void OnEnable()
    {
        propAction = serializedObject.FindProperty("action");
        propIcon = serializedObject.FindProperty("glyphIcon");
        propIconAutoFill = serializedObject.FindProperty("autoFillIcon");
        propPromptTarget = serializedObject.FindProperty("promptTarget");
        propAutoCycle = serializedObject.FindProperty("autoCycle");
        propDuration = serializedObject.FindProperty("duration");
        propSystemPlayer = serializedObject.FindProperty("isSystemPlayer");
        propPlayerNumber = serializedObject.FindProperty("playerNumber");
    }

    public override void OnInspectorGUI()
    {
        //Action
        EditorGUILayout.PropertyField(propAction);

        //Type
        EditorGUILayout.PropertyField(propPromptTarget);
        EditorGUI.indentLevel = 1;
        switch (propPromptTarget.enumValueIndex)
        {
            case 0: // All
                break;
            case 1: // Cycle
                EditorGUILayout.PropertyField(propAutoCycle);
                if(propAutoCycle.boolValue)
                {
                    EditorGUILayout.PropertyField(propDuration);
                }
                break;
            case 2: // Player
                EditorGUILayout.PropertyField(propSystemPlayer);
                if(!propSystemPlayer.boolValue)
                {
                    EditorGUILayout.PropertyField(propPlayerNumber);
                }
                break;

            default:
                break;
        }
        EditorGUI.indentLevel = 0;

        //Icon
        EditorGUILayout.LabelField("Icon", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(propIcon);

        if (propIcon.objectReferenceValue == null) EditorGUILayout.PropertyField(propIconAutoFill);

        serializedObject.ApplyModifiedProperties();
    }

}
