﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerGlyphDisplay : MonoBehaviour {

    public enum TargetContoller {All=0, Player1, Player2, Player3, Player4, SystemPlayer, Cycle}
    public enum PromptTarget {All, Cycle, Player}


    public string action;
    public Image glyphIcon;
    [SerializeField] private bool autoFillIcon;


    [SerializeField] private bool autoCycle;
    [Range(.1f, 5)][SerializeField] private float duration;

    public TargetContoller controllerTarget;
    public PromptTarget promptTarget;

    [SerializeField] private bool isSystemPlayer;
    [Range(1,4)] public int playerNumber;

    private ControllerGlyph currentGlyph;

    private void Awake()
    {
        if(autoFillIcon)
        {
            glyphIcon = GetComponentInChildren<Image>();
        }

        ControllerGlyphManager.OnDisplayUpdate.AddListener(UpdateGlyph);
    }

    private void Update()
    {
        UpdateGlyph();
    }

    void UpdateGlyph()
    {
        if (action == "" || glyphIcon == null) return;

        switch (promptTarget)
        {
            case PromptTarget.All:
                currentGlyph = ControllerGlyphManager.GetLastActiveControllerGlyph(action);
                break;
            case PromptTarget.Cycle:
                currentGlyph = ControllerGlyphManager.GetLastActiveControllerGlyph(action);
                break;
            case PromptTarget.Player:
                if (isSystemPlayer) currentGlyph = ControllerGlyphManager.GetLastActiveControllerGlyph(action, true);
                else currentGlyph = ControllerGlyphManager.GetLastActiveControllerGlyph(action, playerNumber);
                break;
            default:
                break;
        }

        if (currentGlyph == null) return;
        glyphIcon.sprite = currentGlyph.GetGlyph();
    }
    
    //void OnValidate()
    //{
    //    switch (controllerTarget)
    //    {
    //        case TargetContoller.All:
    //            promptTarget = PromptTarget.All;
    //            break;
    //        case TargetContoller.Player1:
    //            promptTarget = PromptTarget.Player;
    //            playerNumber = 1;
    //            break;
    //        case TargetContoller.Player2:
    //            promptTarget = PromptTarget.Player;
    //            playerNumber = 2;
    //            break;
    //        case TargetContoller.Player3:
    //            promptTarget = PromptTarget.Player;
    //            playerNumber = 3;
    //            break;
    //        case TargetContoller.Player4:
    //            promptTarget = PromptTarget.Player;
    //            playerNumber = 4;
    //            break;
    //        case TargetContoller.SystemPlayer:
    //            promptTarget = PromptTarget.Player;
    //            isSystemPlayer = true;
    //            break;
    //        case TargetContoller.Cycle:
    //            promptTarget = PromptTarget.Cycle;
    //            duration = 1;
    //            break;
    //        default:
    //            break;
    //    }
    //}
}
