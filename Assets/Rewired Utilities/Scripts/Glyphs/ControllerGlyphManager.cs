﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Events;
using System.Linq;

public class ControllerGlyphManager : MonoBehaviour {

    private static ControllerGlyphManager instance;
    [SerializeField] private ControllerGlyphSet[] glyphs;
    [SerializeField] private ControllerGlyphSet keyboardGlyph;
    static public UnityEvent OnDisplayUpdate = new UnityEvent();

    private void Awake()
    {
        instance = this;
    }

    public static string GetControllerButtonName(string action, int controllerId)
    {
        Controller lastController = ReInput.controllers.GetController<Joystick>(controllerId);

        ControllerGlyph targetGlyph = GetControllerGlyph(lastController, action);

        return targetGlyph.GetElementName();
    }

    public static string GetLastActiveControllerButtonName(string action, int playerNumber)
    {
        Controller lastController = ReInput.players.GetPlayer(playerNumber).controllers.GetLastActiveController();

        ControllerGlyph targetGlyph = GetControllerGlyph(lastController, action, Pole.Positive, ReInput.players.Players[playerNumber]);

        return targetGlyph.GetElementName();
    }

    public static string GetLastActiveControllerButtonName(string action)
    {
        Controller lastController = ReInput.controllers.GetLastActiveController();

        ControllerGlyph targetGlyph = GetControllerGlyph(lastController, action, Pole.Positive, ReInput.players.SystemPlayer);

        return targetGlyph.GetElementName();
    }

    public static ControllerGlyph GetControllerGlyph(string action, ControllerType controllerType, Pole axisContribution, int controllerId, int playerNumber)
    {
        Controller targetController = ReInput.controllers.GetController(controllerType, controllerId);

        ControllerGlyph targetGlyph = GetControllerGlyph(targetController, action, axisContribution, ReInput.players.GetPlayer(playerNumber));

        return targetGlyph;
    }

    public static ControllerGlyph GetControllerGlyph(string action, int controllerId)
    {
        Controller lastController = ReInput.controllers.GetController<Joystick>(controllerId);

        ControllerGlyph targetGlyph = GetControllerGlyph(lastController, action, Pole.Positive, ReInput.players.SystemPlayer);

        return targetGlyph;
    }

    public static ControllerGlyph GetLastActiveControllerGlyph(string action, int playerNumber)
    {
        Controller lastController = ReInput.players.GetPlayer(playerNumber - 1).controllers.GetLastActiveController();

        if (lastController == null) return null;

        ControllerGlyph targetGlyph = GetControllerGlyph(lastController, action, Pole.Positive, ReInput.players.GetPlayer(playerNumber - 1));

        if (targetGlyph == null) GetLastActiveControllerGlyph(action);

        return targetGlyph;
    }

    public static ControllerGlyph GetLastActiveControllerGlyph(string action, bool isSystemPlayer = false)
    {
        if (isSystemPlayer)
        {
            Controller lastController = ReInput.players.SystemPlayer.controllers.GetLastActiveController();

            if (lastController == null) return null;

            ControllerGlyph targetGlyph = GetControllerGlyph(lastController, action, Pole.Positive, ReInput.players.SystemPlayer);

            return targetGlyph;
        }
        else
        {
            Controller lastController = ReInput.controllers.GetLastActiveController();

            ControllerGlyph targetGlyph = GetControllerGlyph(lastController, action);

            return targetGlyph;
        }
    }

    private ControllerGlyph GetGlyphAction(ControllerGlyphSet glyphSet, string action, Pole axisContribution, Player targetPlayer, Controller targetController)
    {
        if (glyphSet == null)
        {
            Debug.LogError("GlyphSet equals null");
            return null;
        }
        else if (targetPlayer == null)
        {
            Debug.LogError("TargetPlayer equals null");
            return null;
        }

        Controller controller;
        if (targetController.type == ControllerType.Mouse)
        {
            controller = ReInput.controllers.Keyboard;
        }
        else
        {
            controller = targetController;
        }
        

        IEnumerable<ActionElementMap> aemList;
        aemList = targetPlayer.controllers.maps.ElementMapsWithAction(controller.type, action, false).OrderByDescending(s => s.elementIdentifierId);

        ActionElementMap aem = new ActionElementMap();

        for (int i = 0; i < aemList.Count(); i++)
        {
            if(aemList.ElementAt(i).axisContribution == axisContribution)
            {
                aem = aemList.ElementAt(i);
            }
        }

        ControllerGlyph[] glyphArray = glyphSet.GetControllerGlyphs();

        ControllerGlyph targetGlyph = null;
        if (aem == null)
        {
            return targetGlyph;
        }

        for (int i = 0; i < glyphArray.Length; i++)
        {
            if (glyphArray[i].GetElementID() == aem.elementIdentifierId)
            {
                targetGlyph = glyphArray[i];
                break;
            }
        }

        return targetGlyph;
    }

    private ControllerGlyphSet SearchSets(Controller targetController)
    {
        ControllerGlyphSet returningGlyph = null;

        if (targetController.type == ControllerType.Joystick)
        {
            string targetGUID = ((Joystick)targetController).hardwareTypeGuid.ToString();

            for (int i = 0; i < glyphs.Length; i++)
            {
                if (targetGUID == glyphs[i].GUID)
                {
                    returningGlyph = glyphs[i];
                    break;
                }
            }
        }
        else if (targetController.type == ControllerType.Keyboard)
        {
            returningGlyph = keyboardGlyph;
        }
        else if (targetController.type == ControllerType.Mouse)
        {
            returningGlyph = keyboardGlyph;
        }

        return returningGlyph;
    }

    private static ControllerGlyph GetControllerGlyph(Controller targetController, string action)
    {
        if (targetController == null) return null;
        ControllerGlyph targetGlyph = null;

        Player targetPlayer = null;

        for (int i = 0; i < ReInput.players.playerCount; i++)
        {
            if (ReInput.controllers.IsControllerAssignedToPlayer(targetController.type, targetController.id, i))
            {
                targetPlayer = ReInput.players.GetPlayer(i);
            }
        }

        if (targetPlayer == null) targetPlayer = ReInput.players.SystemPlayer;

        targetGlyph = GetControllerGlyph(targetController, action, Pole.Positive, targetPlayer);

        return targetGlyph;
    }

    private static ControllerGlyph GetControllerGlyph(Controller targetController, string action, Pole axisContribution, Player targetPlayer)
    {
        if (targetController == null)
        {
            Debug.LogError("Target Controller equals null");
            return null;
        }
        ControllerGlyphSet targetSet = null;

        if (targetController != null)
        {
            targetSet = instance.SearchSets(targetController);
        }

        ControllerGlyph targetGlyph = null;

        targetGlyph = instance.GetGlyphAction(targetSet, action, axisContribution, targetPlayer, targetController);

        return targetGlyph;
    }
}
