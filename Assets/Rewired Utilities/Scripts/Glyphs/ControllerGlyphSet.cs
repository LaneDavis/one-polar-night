﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Controller Glyph Set")]
public class ControllerGlyphSet : ScriptableObject {

    [SerializeField] private string controllerName;
    [SerializeField] private string guid;
    [SerializeField] private ControllerGlyph[] controllerGlyphs;
    
    public string ControllerName
    {
        get
        {
            return controllerName;
        }
    }

    public string GUID
    {
        get
        {
            return guid;
        }
    }

    public ControllerGlyph[] GetControllerGlyphs()
    {
        return controllerGlyphs;
    }
}
