﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ControllerGlyph {

    [SerializeField] private string elementName;
    [SerializeField] private int elementID;
    [SerializeField] private Sprite glyphImage;
    
    public string GetElementName()
    {
        return elementName;
    }

    public int GetElementID()
    {
        return elementID;
    }

    public Sprite GetGlyph()
    {
        return glyphImage;
    }
}
