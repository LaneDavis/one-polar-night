﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Rewired.ControllerExtensions;


public class ControllerManagerPS4Colors : MonoBehaviour {

    public Color[] playerColors = new Color[4] {    new Color(0, .7f, 1, 1),
                                                    new Color(1, 0, 0, 1),
                                                    new Color(0, 1, 0, 1),
                                                    new Color(1, 0, .8f, 1) };

    private Color[] playerLightColor = new Color[4] {new Color(0, .7f, 1, 1),
                                                    new Color(1, 0, 0, 1),
                                                    new Color(0, 1, 0, 1),
                                                    new Color(1, 0, .8f, 1)};

    [SerializeField] private AnimationCurve transitionCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    [Header("Flash")]
    [SerializeField] private AnimationCurve flashCurve = new AnimationCurve(new Keyframe(0, 0),new Keyframe(.2f, 1), new Keyframe(1, 0));
    [SerializeField] private float defaultFlashDuration = .4f;

    [Header("Pulse")]
    [SerializeField] private AnimationCurve pulseCurve = new AnimationCurve(new Keyframe(0, 1), new Keyframe(.5f, 0), new Keyframe(1, 1));

    private Color defaultColor = new Color(1, 1, 1, .5f);

    static private ControllerManagerPS4Colors instance;
    static private Coroutine transitionCoroutine;
    static private Coroutine flashCoroutine;
    static private Coroutine pulseCoroutine;
    static private Coroutine lightCoroutine;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        UpdateAllPS4Controllers();
    }

    static public void UpdateAllPS4Controllers()
    {
        if (ReInput.controllers.GetControllers(ControllerType.Joystick) == null) return;
        foreach (Controller controller in ReInput.controllers.GetControllers(ControllerType.Joystick))
        {
            DualShock4Extension ds4 = controller.GetExtension<DualShock4Extension>();
            if (ds4 == null) continue;

            instance.SetControllerColor(controller, ds4);
        }
    }

    void SetControllerColor(Controller controller, DualShock4Extension ds4)
    {
        for (int i = 0; i < ReInput.players.playerCount; i++)
        {
            if (ReInput.controllers.IsControllerAssignedToPlayer(controller.type, controller.id, i))
            {
                ds4.SetLightColor(playerColors[i]);
                return;
            }
        }

        ds4.SetLightColor(defaultColor);
    }

    void SetLightColor(int playerNumber, Color color)
    {
        List<DualShock4Extension> list = GetPlayerPS4Controllers(playerNumber);

        if (list == null) return;

        for (int i = 0; i < list.Count; i++)
        {
            list[i].SetLightColor(color);
        }

        playerLightColor[playerNumber - 1] = color;
    }

    void SetPlayerColor(int playerNumber, Color color)
    {
        SetLightColor(playerNumber, color);

        playerColors[playerNumber - 1] = color;
    }

    static public void ChangePlayerColor(int playerNumber, Color newColor, float duration)
    {
        if(transitionCoroutine != null) instance.StopCoroutine(transitionCoroutine);
        transitionCoroutine = instance.StartCoroutine(instance.ChangeColor(playerNumber, newColor, duration));
    }

    static public void FlashPlayerColor(int playerNumber, Color newColor)
    {
        if (flashCoroutine != null) instance.StopCoroutine(flashCoroutine);
        flashCoroutine = instance.StartCoroutine(instance.FlashColor(playerNumber, newColor));
    }

    static public void PulsePlayerColor(int playerNumber, float minIntensity, float maxIntensity, float ratePerSec)
    {
        Color color1 = GetPlayerColorIntensity(playerNumber, minIntensity);
        Color color2 = GetPlayerColorIntensity(playerNumber, maxIntensity);

        PulsePlayerColor(playerNumber, color1, color2, ratePerSec);
    }

    static public void PulsePlayerColor(int playerNumber, Color color1, Color color2, float ratePerSec)
    {
        if (pulseCoroutine != null) instance.StopCoroutine(pulseCoroutine);
        pulseCoroutine = instance.StartCoroutine(instance.PulseColor(playerNumber, color1, color2, ratePerSec));
    }

    static public void PulsePlayerColor(int playerNumber, float minIntensity, float maxIntensity, float ratePerSec, float duration)
    {
        Color color1 = GetPlayerColorIntensity(playerNumber, minIntensity);
        Color color2 = GetPlayerColorIntensity(playerNumber, maxIntensity);

        PulsePlayerColor(playerNumber, color1, color2, ratePerSec, duration);
    }

    static public void PulsePlayerColor(int playerNumber, Color color1, Color color2, float ratePerSec, float duration)
    {
        if (pulseCoroutine != null) instance.StopCoroutine(pulseCoroutine);
        pulseCoroutine = instance.StartCoroutine(instance.PulseColor(playerNumber, color1, color2, ratePerSec, duration));
    }

    static public void PulsePlayerColorStop(int playerNumber)
    {
        if (pulseCoroutine == null) return;
        instance.StopCoroutine(pulseCoroutine);
        ResetPlayerLightColor(playerNumber, .3f);
    }

    static private void ResetPlayerLightColor(int playerNumber, float duration)
    {
        if (lightCoroutine != null) instance.StopCoroutine(lightCoroutine);
        lightCoroutine = instance.StartCoroutine(instance.ResetLightColor(playerNumber, duration));
    }

    IEnumerator ChangeColor(int playerNumber, Color targetColor, float duration )
    {
        Color originalColor = playerColors[playerNumber - 1];

        float timer = 0;
        float percent = 0;
        Color newColor;

        while(timer < duration)
        {
            timer += Time.unscaledDeltaTime;
            percent = timer / duration;

            newColor = Color.Lerp(originalColor, targetColor, transitionCurve.Evaluate(percent));

            SetPlayerColor(playerNumber, newColor);

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator ResetLightColor(int playerNumber, float duration)
    {
        if (playerNumber > playerLightColor.Length || playerNumber <= 0)
        {
            yield break;
        }

        float timer = 0;
        float percent = 0;
        Color newColor;
        Color originalColor = playerLightColor[playerNumber - 1];

        Color targetColor = playerColors[playerNumber - 1];

        while (timer < duration)
        {
            timer += Time.unscaledDeltaTime;
            percent = timer / duration;

            newColor = Color.Lerp(originalColor, targetColor, transitionCurve.Evaluate(percent));

            SetLightColor(playerNumber, newColor);

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator FlashColor(int playerNumber, Color targetColor)
    {
        Color originalColor = playerColors[playerNumber - 1];
        float timer = 0;
        float percent = 0;
        float duration = defaultFlashDuration;
        Color newColor;

        while (timer < duration)
        {
            timer += Time.unscaledDeltaTime;
            percent = timer / duration;

            newColor = Color.Lerp(originalColor, targetColor, flashCurve.Evaluate(percent));

            SetLightColor(playerNumber, newColor);

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator PulseColor(int playerNumber, Color color1, Color color2, float ratePerSec, float duration)
    {
        float timer = 0;
        float pulseTimer = 0;
        float pulsePercent = 0;

        Color newColor = playerColors[playerNumber - 1];

        pulseCurve.postWrapMode = WrapMode.Loop;

        while (timer < duration)
        {
            timer += Time.unscaledDeltaTime;

            pulseTimer += Time.unscaledDeltaTime * ratePerSec;
            pulsePercent = pulseTimer / 1;

            newColor = Color.Lerp(color1, color2, pulseCurve.Evaluate(pulsePercent));

            SetLightColor(playerNumber, newColor);

            yield return new WaitForEndOfFrame();
        }

        ResetPlayerLightColor(playerNumber, .3f);
    }

    IEnumerator PulseColor(int playerNumber, Color color1, Color color2, float ratePerSec)
    {
        float pulseTimer = 0;
        float pulsePercent = 0;

        Color newColor;

        pulseCurve.postWrapMode = WrapMode.Loop;

        while (true)
        {
            pulseTimer += Time.unscaledDeltaTime * ratePerSec;
            pulsePercent = pulseTimer / 1;

            newColor = Color.Lerp(color1, color2, pulseCurve.Evaluate(pulsePercent));

            SetLightColor(playerNumber, newColor);

            yield return new WaitForEndOfFrame();
        }
    }

    static Color GetPlayerColorIntensity(int playerNumber, float intensity)
    {
        Color playerColor = instance.playerColors[playerNumber - 1];

        intensity = Mathf.Clamp01(intensity);

        Color newColor = new Color(playerColor.r, playerColor.g, playerColor.b, intensity);

        return newColor;
    }

    void OnApplicationQuit()
    {
        if (ReInput.controllers.GetControllers(ControllerType.Joystick) == null) return;
        foreach (Controller controller in ReInput.controllers.GetControllers(ControllerType.Joystick))
        {
            DualShock4Extension ds4 = controller.GetExtension<DualShock4Extension>();
            if (ds4 == null) continue;
            ds4.SetLightColor(new Color(1, 1, 1, .1f));
        }
    }

    List<DualShock4Extension> GetPlayerPS4Controllers(int playerNumber)
    {
        List<DualShock4Extension> controllers = new List<DualShock4Extension>();

        if (ReInput.controllers.GetControllers(ControllerType.Joystick) == null) return null;
        foreach (Controller controller in ReInput.controllers.GetControllers(ControllerType.Joystick))
        {
            if(ReInput.controllers.IsControllerAssignedToPlayer(ControllerType.Joystick, controller.id, ReInput.players.Players[playerNumber-1].id))
            {
                DualShock4Extension ds4 = controller.GetExtension<DualShock4Extension>();
                if (ds4 != null) controllers.Add(ds4);
            }
        }

        return controllers;
    }
}
