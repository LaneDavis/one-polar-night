﻿using UnityEngine;
using System.Collections;
using Rewired;
using Rewired.ControllerExtensions;

public class ControllerManager : MonoBehaviour {

    void Start()
    {
        //ReInput.ControllerConnectedEvent += ReInput_ControllerConnectedEvent;
        //AssignControllersToSystemPlayer();
    }

    private void ReInput_ControllerConnectedEvent(ControllerStatusChangedEventArgs obj)
    {
        //AssignControllersToSystemPlayer();
    }

    static public void AssignControllersToSystemPlayer()
    {
        ReInput.players.SystemPlayer.controllers.hasKeyboard = true;
        if (ReInput.controllers.GetControllerCount(ControllerType.Joystick) != 0)
        {
            foreach (Controller targetController in ReInput.controllers.GetControllers(ControllerType.Joystick))
            {
                if (!ReInput.players.GetPlayer("SYSTEM").controllers.ContainsController(targetController.type, targetController.id) && !IsAssignedToPlayers(targetController))
                {
                    ReInput.players.SystemPlayer.controllers.AddController(targetController, false);
                }
            }
        }
        
        if(ReInput.controllers.GetControllerCount(ControllerType.Custom) != 0)
        {
            foreach (Controller targetController in ReInput.controllers.GetControllers(ControllerType.Custom))
            {
                if (!ReInput.players.GetPlayer("SYSTEM").controllers.ContainsController(targetController.type, targetController.id) && !IsAssignedToPlayers(targetController))
                {
                    ReInput.players.SystemPlayer.controllers.AddController(targetController, false);
                }
            }
        }

        ControllerManagerPS4Colors.UpdateAllPS4Controllers();
    }

    static public bool IsAssignedToPlayer(Controller controller, Player player)
    {
        if (ReInput.controllers.IsControllerAssignedToPlayer(controller.type, controller.id, player.id)) return true;
        else return false;
    }

    static public bool IsAssignedToPlayers(Controller controller)
    {
        //if (controller.type == ControllerType.Keyboard) return false;

        if (ReInput.controllers.IsControllerAssignedToPlayer(controller.type, controller.id, 0)) return true;
        if (ReInput.controllers.IsControllerAssignedToPlayer(controller.type, controller.id, 1)) return true;
        if (ReInput.controllers.IsControllerAssignedToPlayer(controller.type, controller.id, 2)) return true;
        if (ReInput.controllers.IsControllerAssignedToPlayer(controller.type, controller.id, 3)) return true;

        return false;
    }
}
