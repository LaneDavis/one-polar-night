﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class RebindPlayerDisplay : MonoBehaviour {

    public bool showSystemPlayer = false;
    private OptionList list;
    IList<Player> players;

    private void Awake()
    {
        list = GetComponentInChildren<OptionList>();
        list.onValueChanged.AddListener(ChangePlayer);

        if (showSystemPlayer) players = ReInput.players.AllPlayers;
        else players = ReInput.players.Players;
    }

    private void OnEnable()
    {
        SetupDisplay();
    }

    void SetupDisplay()
    {
        list.options.Clear();

        for(int i = 0; i < players.Count; i++)
        {
            if(players[i].isPlaying) list.options.Add(players[i].descriptiveName);
        }
    }

    public void ChangePlayer(int playerIndex)
    {
        RebindManager.instance.ChangePlayer(players[playerIndex].id);
    }
}
