﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RebindPromptDisplay : MonoBehaviour {

    public TextMeshProUGUI prompt;
    public TextMeshProUGUI timer;
    public Image bar;

    private void Awake()
    {
        RebindManager.inputMapper.StartedEvent += UpdatePrompt;
    }

    private void UpdatePrompt(Rewired.InputMapper.EventData obj)
    {
        UpdatePrompt();
    }

    private void OnEnable()
    {
        UpdatePrompt();
    }

    private void Update()
    {
        float percent = RebindManager.inputMapper.timeRemaining / RebindManager.inputMapper.options.timeout;

        timer.text = RebindManager.inputMapper.timeRemaining.ToString("#");
        bar.fillAmount = percent;
    }

    public void UpdatePrompt()
    {
        prompt.text = "Press \"" + RebindManager.targetActionDescriptive + "\" to Rebind";
    }
}
