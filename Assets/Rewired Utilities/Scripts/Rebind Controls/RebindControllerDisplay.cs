﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class RebindControllerDisplay : MonoBehaviour {

    private OptionList list;
    Controller[] controllers;
    List<ControllerDisplayHelper> displayedControllers = new List<ControllerDisplayHelper>();
    List<string> controllerNames = new List<string>();

    private void Awake()
    {
        list = GetComponentInChildren<OptionList>();
        RebindManager.OnPlayerUpdate.AddListener(UpdateDisplay);
        ReInput.ControllerConnectedEvent += UpdateDisplay;
        ReInput.ControllerDisconnectedEvent += UpdateDisplay;
    }

    private void UpdateDisplay(ControllerStatusChangedEventArgs obj)
    {
        UpdateDisplay();
    }

    void SetupDisplay()
    {
        list.options.Clear();
        displayedControllers.Clear();
        controllerNames.Clear();

        controllers = ReInput.controllers.GetControllers(ControllerType.Joystick);
        NameControllers();

        int startingIndex = 0;
        list.options.Add("None");

        if (controllers == null || controllers.Length == 0) return;

        for (int i = 0; i < controllers.Length; i++)
        {
            if (!ControllerManager.IsAssignedToPlayers(controllers[i]) || ControllerManager.IsAssignedToPlayer(controllers[i], RebindManager.currentPlayer))
            {
                list.options.Add(controllerNames[i]);
                displayedControllers.Add(new ControllerDisplayHelper(i, controllers[i], controllerNames[i]));
            }

            if (RebindManager.currentPlayer.controllers.ContainsController(controllers[i])) startingIndex = i + 1;
        }

        SetListValueNoCall(startingIndex);
    }

    public void AssignController(int controllerIndex)
    {
        if (controllers == null || controllers.Length == 0) return;
        if (controllerIndex <= 0) RebindManager.instance.RemoveControllers();
        else RebindManager.instance.AssignController(displayedControllers[controllerIndex-1].controller, true);
    }

    void UpdateDisplay()
    {
        SetupDisplay();
        if (controllers == null || controllers.Length == 0)
        {
            SetListValueNoCall(0);
            return;
        }

        for (int i = 0; i < controllers.Length; i++)
        {
            if(RebindManager.currentPlayer.controllers.ContainsController(controllers[i]))
            {
                SetListValueNoCall(i+1);
                return;
            }
        }

        SetListValueNoCall(0);
    }

    void SetListValueNoCall(int value)
    {
        list.onValueChanged.RemoveListener(AssignController);
        list.Value = value;
        list.onValueChanged.AddListener(AssignController);
    }

    void NameControllers()
    {
        if (controllers == null || controllers.Length == 0) return;
        for (int i = 0; i < controllers.Length; i++)
        {
            string controllerName = controllers[i].name;
            int numberOfSameControllers = 0;
            for (int j = 0; j < i; j++)
            {
                if (controllers[j].name == controllerName) numberOfSameControllers++;
            }

            if (numberOfSameControllers > 0) controllerName += " (" + (numberOfSameControllers + 1) + ")";
            else
            {
                for (int j = i+1; j < controllers.Length; j++)
                {
                    if (controllers[j].name == controllerName)
                    {
                        controllerName += " (1)";
                        break;
                    }
                }
            }

            controllerNames.Add(controllerName);
        }
    }

    public class ControllerDisplayHelper
    {
        public int controllerIndex;
        public Controller controller;
        public string controllerName;

        public ControllerDisplayHelper(int index, Controller control, string name)
        {
            controllerIndex = index;
            controller = control;
            controllerName = name;
        }
    }
}
