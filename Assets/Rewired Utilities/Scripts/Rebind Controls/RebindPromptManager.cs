﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class RebindPromptManager : MonoBehaviour {

    private MenuSystemManager menu;
    public string targetMenu;

    public void Awake()
    {
        menu = GetComponentInParent<MenuSystemManager>();

        RebindManager.inputMapper.StartedEvent += DisplayPrompt;
        RebindManager.inputMapper.TimedOutEvent += HidePrompt;
        //RebindManager.inputMapper.StoppedEvent += HidePrompt;
        RebindManager.inputMapper.CanceledEvent += HidePrompt;
        RebindManager.inputMapper.ErrorEvent += HidePrompt;

        RebindManager.OnControlListMapped.AddListener(HidePrompt);
    }

    private void HidePrompt()
    {
        menu.LastOpenedMenu();
    }

    private void HidePrompt(InputMapper.EventData obj)
    {
        if(obj.inputMapper.status != InputMapper.Status.Listening) HidePrompt();
    }

    private void DisplayPrompt(InputMapper.StartedEventData obj)
    {
        menu.SwitchMenu(targetMenu, true);
    }
}
