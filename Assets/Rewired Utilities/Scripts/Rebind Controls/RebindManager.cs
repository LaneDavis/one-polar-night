﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Rewired;
using System.Linq;

public class RebindManager : MonoBehaviour {

    static public RebindManager instance;

    static public InputMapper inputMapper;

    static public Player currentPlayer;
    static private int playerIndex;

    static public UnityEvent OnPlayerUpdate = new UnityEvent();
    static public UnityEvent OnControllerUpdate = new UnityEvent();
    static public UnityEvent OnControlMapped = new UnityEvent();
    static public UnityEvent OnControlListMapped = new UnityEvent();
    static public UnityEvent OnResetDefaults = new UnityEvent();

    public Sprite unassignedSprite;
    static public string targetAction = "";
    static public string targetActionDescriptive = "";
    static private int numberToReassign = 0;

    private bool hasReassigned = false;

    public void Awake()
    {
        inputMapper = new InputMapper();

        inputMapper.options.timeout = 10f;
        inputMapper.options.ignoreMouseXAxis = true;
        inputMapper.options.ignoreMouseYAxis = true;
        inputMapper.options.allowKeyboardKeysWithModifiers = false;
        inputMapper.options.checkForConflicts = true;
        inputMapper.options.allowAxes = true;
        inputMapper.options.allowButtons = true;
        
        inputMapper.InputMappedEvent += OnInputMapped;
        inputMapper.StoppedEvent += OnInputStopped;
        inputMapper.CanceledEvent += StopList;
        inputMapper.ErrorEvent += StopList;
        inputMapper.ConflictFoundEvent += StopList;

        instance = this;
    }

    private void StopList(InputMapper.EventData obj)
    {
        if (inputMapper.status != InputMapper.Status.Listening)
        {
            numberToReassign = 0;
            StopAllCoroutines();
        }
    }

    private void OnInputStopped(InputMapper.StoppedEventData obj)
    {
        ReInput.userDataStore.SavePlayerData(currentPlayer.id);
        if (numberToReassign == 0) OnControlListMapped.Invoke();
    }

    private void OnInputMapped(InputMapper.InputMappedEventData obj)
    {
        inputMapper.Stop();
        OnControlMapped.Invoke();
        Controller controller = ReInput.controllers.GetLastActiveController();

        if (controller.type == ControllerType.Keyboard)
        {
            if (obj.actionElementMap.axisContribution == Pole.Positive)
            {
                StartCoroutine(DelayRebind());
                hasReassigned = false;
            }
            else
            {
                hasReassigned = true;
            }
        }
        else
        {
            hasReassigned = true;
        }

        
    }

    IEnumerator DelayRebind()
    {
        yield return new WaitForEndOfFrame();
        RebindControl(targetAction, Pole.Negative);
    }

    public void ChangePlayer(int playerID)
    {
        playerIndex = playerID;
        currentPlayer = ReInput.players.GetPlayer(playerIndex);
        OnPlayerUpdate.Invoke();
    }

    public void AssignController(Controller controller, bool removeOtherControllers = true)
    {
        if (removeOtherControllers) currentPlayer.controllers.ClearControllersOfType(controller.type);

        currentPlayer.controllers.AddController(controller, true);
        OnControllerUpdate.Invoke();
    }

    public void RemoveControllers()
    {
        currentPlayer.controllers.ClearControllersOfType(ControllerType.Joystick);
        ControllerManager.AssignControllersToSystemPlayer();
        OnControllerUpdate.Invoke();
    }

    public void RebindControl(string action, Pole axisContribution = Pole.Positive)
    {
        Controller controller = ReInput.controllers.GetLastActiveController();
        //ActionElementMap aem = currentPlayer.controllers.maps.GetFirstElementMapWithAction(controller, action, true);

        IEnumerable<ActionElementMap> aemList;
        aemList = currentPlayer.controllers.maps.ElementMapsWithAction(controller.type, action, false).OrderByDescending(s => s.elementIdentifierId);

        ActionElementMap aem = new ActionElementMap();

        for (int i = 0; i < aemList.Count(); i++)
        {
            if (aemList.ElementAt(i).axisContribution == axisContribution)
            {
                aem = aemList.ElementAt(i);
            }
        }

        InputAction inputAction = ReInput.mapping.GetAction(action);

        ControllerMap map = currentPlayer.controllers.maps.GetMap(controller, aem.controllerMap.categoryId, aem.controllerMap.layoutId);

        Debug.Log(aem.elementIdentifierName);

        AxisRange range = AxisRange.Full;
        if(controller.type == ControllerType.Keyboard)
        {
            if (axisContribution == Pole.Negative) range = AxisRange.Negative;
            else range = AxisRange.Positive;
        }
        else if(controller.type == ControllerType.Joystick)
        {
            range = AxisRange.Full;
        }
        

        InputMapper.Context context = new InputMapper.Context()
        {
            actionId = aem.actionId,
            actionRange = range,
            controllerMap = map,
            actionElementMapToReplace = aem,
        };

        targetAction = action;
        if (inputAction.type == InputActionType.Button || controller.type == ControllerType.Joystick) targetActionDescriptive = inputAction.descriptiveName;
        else
        {
            if (axisContribution == Pole.Positive) targetActionDescriptive = inputAction.positiveDescriptiveName;
            else targetActionDescriptive = inputAction.negativeDescriptiveName;
        }

        inputMapper.Start(context);
    }

    public void RebindControl(string[] actions)
    {
        StartCoroutine(RebindList(actions));
    }

    IEnumerator RebindList(string[] actions)
    {
        numberToReassign = actions.Length;
        
        for (int i = 0; i < actions.Length; i++)
        {
            RebindControl(actions[i]);
            hasReassigned = false;
            while (!hasReassigned)
            {
                yield return new WaitForEndOfFrame();
            }
            
            numberToReassign--;
            Controller controller = ReInput.controllers.GetLastActiveController();
            InputAction inputAction = ReInput.mapping.GetAction(actions[i]);
            if (controller.type == ControllerType.Joystick && inputAction.type == InputActionType.Axis) yield return new WaitForSecondsRealtime(.5f);
        }

        hasReassigned = false;
        OnControlListMapped.Invoke();
    }

    static public Controller GetPlayerController(ControllerType controllerType)
    {
        Controller controller = null;

        if (controllerType == ControllerType.Joystick)
        {
            for (int i = 0; i < currentPlayer.controllers.joystickCount; i++)
            {
                if (currentPlayer.controllers.Joysticks[i] != null)
                {
                    controller = currentPlayer.controllers.Joysticks[i];
                    break;
                }
            }
        }
        else if (controllerType == ControllerType.Keyboard)
        {
            controller = currentPlayer.controllers.GetController(controllerType, 0);
        }

        return controller;
    }

    static public void ResetBindings()
    {
        currentPlayer.controllers.maps.LoadDefaultMaps(ControllerType.Keyboard);
        currentPlayer.controllers.maps.LoadDefaultMaps(ControllerType.Joystick);

        ReInput.userDataStore.SavePlayerData(currentPlayer.id);

        OnResetDefaults.Invoke();
    }
}
