﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class RebindGlyphDisplay : MonoBehaviour {

    public string action;
    public Image glyphIcon;
    [SerializeField] private bool autoFillIcon;

    public bool useRebindManager = true;
    public ControllerType controllerType;
    public Pole axisContribution = Pole.Positive;

    private ControllerGlyph currentGlyph;

    private void Awake()
    {
        RebindManager.OnPlayerUpdate.AddListener(UpdateDisplay);
        RebindManager.OnControllerUpdate.AddListener(UpdateDisplay);
        RebindManager.OnControlMapped.AddListener(UpdateDisplay);
        RebindManager.OnResetDefaults.AddListener(UpdateDisplay);

        if (autoFillIcon) glyphIcon = GetComponentInChildren<Image>();
    }

    private void OnEnable()
    {
        UpdateDisplay();
    }

    public void UpdateDisplay()
    {
        if (RebindManager.currentPlayer == null || (controllerType == ControllerType.Joystick && RebindManager.currentPlayer.controllers.joystickCount == 0))
        {
            glyphIcon.sprite = RebindManager.instance.unassignedSprite;
            return;
        }

        Player player = RebindManager.currentPlayer;
        Controller controller = RebindManager.GetPlayerController(controllerType);

        ControllerGlyph glyph = ControllerGlyphManager.GetControllerGlyph(action, controllerType, axisContribution, controller.id, player.id);
        Sprite glyphSprite;

        if (glyph == null) glyphSprite = RebindManager.instance.unassignedSprite;
        else glyphSprite = glyph.GetGlyph();

        glyphIcon.sprite = glyphSprite;
    }
}
