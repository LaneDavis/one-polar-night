﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item Database", menuName = "Inventory/Item Database", order = 3)]
public class ItemDatabase : ScriptableObject {

    public List<InventoryItem> Items = new List<InventoryItem>();
    public List<ItemCombination> Combinations = new List<ItemCombination>();

    public ItemCombination CombinationFromItems(InventoryItem item1, InventoryItem item2) {
        foreach (ItemCombination combo in Combinations) {
            if (combo.inputs[0] == item1 && combo.inputs[1] == item2) {
                return combo;
            }

            if (combo.inputs[1] == item1 && combo.inputs[0] == item2) {
                return combo;
            }
        }

        return null;
    }
    
}
