﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item Combination", menuName = "Inventory/Item Combination", order = 2)]
public class ItemCombination : ScriptableObject {

    [Header("Input")] public InventoryItem[] inputs = new InventoryItem[2];
    [Header("Output")] public InventoryItem output;

}
