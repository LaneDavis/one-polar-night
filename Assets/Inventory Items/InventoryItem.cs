﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Inventory Item", order = 1)]
public class InventoryItem : ScriptableObject {

    public string DisplayName;
    public string Description;
    public Sprite Sprite;

}
